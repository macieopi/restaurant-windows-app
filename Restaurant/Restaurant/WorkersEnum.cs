﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Restaurant
{
    public class Workers
    {
        public enum Worker
        {
            Waiter,
            Host,
            Soucier,
            SousChef,
            Chef
        }

        public IList<Worker> ListOfWorkers
        {
            get
            {
                return Enum.GetValues(typeof(Worker)).Cast<Worker>().ToList();
            }
        }

    }
}
