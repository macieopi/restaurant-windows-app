﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Restaurant
{
    [Serializable]
    public class Soucier : Employee
    {
        public Soucier(int id) : base(id)
        {
            Multiplier = 1;
            Position = "Soucier";
        }
    }
}
