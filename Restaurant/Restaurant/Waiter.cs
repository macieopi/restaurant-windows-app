﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Restaurant
{
    [Serializable]
    public class Waiter:Employee
    {
        public Waiter(int id) : base(id)
        {
        Multiplier = 2;
        Position="Waiter";
        }
    }
}
