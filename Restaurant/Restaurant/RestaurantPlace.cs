﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Threading.Tasks;

namespace Restaurant
{
    
    [Serializable]
    public class RestaurantPlace
    {
        public List<Employee> Employees=new List<Employee>();
        public float DailyIncomes { get; private set; }
        public float Incomes { get; private set; }
        private int _counterOfID;
        public int NumberOfDay { get; set; }
        public bool Closed;
       public RestaurantPlace()
        {
            Incomes = 0;
            _counterOfID = 1;
            NumberOfDay = 0;
            Closed = true;
        }
        public void Rectuit(Workers.Worker e)
        {            
            
            if(e== Workers.Worker.Waiter)
          Employees.Add(new Waiter(_counterOfID++));
            if (e == Workers.Worker.Chef)
                Employees.Add(new Chef(_counterOfID++));
            if (e == Workers.Worker.Host)
                Employees.Add(new Host(_counterOfID++));
            if (e == Workers.Worker.Soucier)
                Employees.Add(new Soucier(_counterOfID++));
            if (e == Workers.Worker.SousChef)
                Employees.Add(new SousChef(_counterOfID++));

        }
        public void FireSomeone(Employee employee)
        {
            Employees.Remove(employee);
        }
        public void ShowEmployee()
        {
            foreach (Employee v in Employees)
            {
                MessageBox.Show(v.Position+v.ID+" "+v.Revenue);
            }
        }
        public void GetMeal(float cost)
        {
            //MessageBox.Show("Dinner is served! Enjoy your meal!");
            DailyIncomes += cost;
        }
        private void RevenueShare()
        {
            float temp = 0;
            float revenue;
            foreach(Employee v in Employees)
	        {
                temp += v.Multiplier;
            }
            revenue = (DailyIncomes * 0.7f) / temp;

            foreach (Employee v in Employees)
            {
                v.GetPaid(revenue);
            }
            Incomes += (DailyIncomes * 0.3f);
           Incomes= (float)Math.Round(Incomes, 2);
        }
        public void StartDay(float cost)
        {
       
            DailyIncomes = 0;
            GetMeal(cost);

            foreach (Employee v in Employees)
                v.DoJob();
        }
        public void EndDay()
        {
            NumberOfDay++;
            RevenueShare();
        }
    }
}
