﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Restaurant
{
    [Serializable]
    class Chef:Employee
    {
        public Chef(int id) : base(id)
        {
            Multiplier = 3;
            Position = "Chef";
        }
    }
}
