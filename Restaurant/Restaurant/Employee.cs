﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;


namespace Restaurant
{
    [Serializable]
    public abstract class Employee : Person
    {
        public string Position { get; internal set; }
        public float Revenue { get; set ;} 
        public float Multiplier { get; internal set; }

        public Employee(int id):base(id)
        {
            Position = "";
            Revenue = 0;
            
        }

        public void GetPaid(float salary)
        {
            Revenue += (salary * Multiplier);
            Revenue=(float)Math.Round(Revenue, 2);
        }

        public void DoJob()
        {
           // MessageBox.Show("Doing my " + Position + " job - " + ID);
            //std::cout << "Doing my " << _position << " job - " << _iD << std::endl;
        }



    }
}
