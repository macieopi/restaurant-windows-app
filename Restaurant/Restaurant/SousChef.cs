﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Restaurant
{
    [Serializable]
    public class SousChef : Employee
    {
        public SousChef(int id) : base(id)
        {
            Multiplier = 2;
            Position = "SousChef";
        }
    }
}
