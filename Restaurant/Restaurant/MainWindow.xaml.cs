﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Threading;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;

// TO DO LIST : ocasionaly robber, 

namespace Restaurant
{
    /// <summary>
    /// Logika interakcji dla klasy MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        RestaurantPlace restaurant; 
        Random IncomesToRestaurant = new Random();
        Random tips = new Random();
        private float Tips;
        
       
        
        
        public MainWindow()
        {
          
            LoadFile();
            InitializeComponent();
            Default();
            if (!restaurant.Closed)
                DayButtonPropertiesChange();
        }

      

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!restaurant.Closed)
            {
                restaurant.EndDay();
                restaurant.Closed = true;
            }

            MessageBoxResult dialog = MessageBox.Show("Do you want to save your progress?", "Exit", MessageBoxButton.YesNo);
            if(dialog==MessageBoxResult.Yes)
            {
                SaveFile();
            }
        }
        private void SaveFile()
        {
            Stream fs = new FileStream("State.data", FileMode.Create);
            IFormatter formatter = new BinaryFormatter();
            try
            {
                formatter.Serialize(fs, restaurant);

            }
            catch (Exception e)
            {
                Console.WriteLine("Failed to serialize. Reason: " + e.Message);
                throw;
            }
            finally
            {
                fs.Close();
            }
        }
        private void LoadFile()
        {
            
                    restaurant = null;
                if (File.Exists("State.data"))
                {
                      FileStream fs = new FileStream("State.data", FileMode.Open);
                        MessageBoxResult dialog = MessageBox.Show("Do you want to load your progress?", "Progress", MessageBoxButton.YesNo);
                    if (dialog == MessageBoxResult.Yes)
                    {
                        try
                        {
                            IFormatter formatter = new BinaryFormatter();
                            restaurant = (RestaurantPlace)formatter.Deserialize(fs);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("Failed to deserialize. Reason: " + e.Message);
                            throw;
                        }
                        finally
                        {
                            fs.Close();
                        }
                    }
                    if (dialog == MessageBoxResult.No)
                    {
                        restaurant = new RestaurantPlace();
                        fs.Close();
                    }
                }
                else
                {
                    restaurant = new RestaurantPlace();
                }
            
            
        }
        private void Default()
        {
            //Closed is default
            DayNumber.Content = "Day: " + restaurant.NumberOfDay;
            StartDayButton.IsEnabled = true;
            EndDayButton.IsEnabled = false;
            HireButton.IsEnabled = true;
            RemoveButton.IsEnabled = true;
            DayTime.Value = 0;
            Tips = 0;
            InfoLabel.Content = " ";
            ListOfEmployee.ItemsSource = restaurant.Employees;
            StatusLabel.Content = "Closed";
            RestaurantIncomes.Content = "Restaurant incomes: " + restaurant.Incomes + " $ ";
            TipsLabel.Content = "Tips: " + Tips + " $ ";

            ICollectionView view = CollectionViewSource.GetDefaultView(ListOfEmployee.ItemsSource);
            view.Refresh();
        }
        private async void DayProgress()
        {
            bool end = false;
            var progress = new Progress<int>(value => DayTime.Value = value);
            await Task.Run(() =>
            {
                for (int i = 0; !restaurant.Closed  && i < 1000; i++)
                {
                    ((IProgress<int>)progress).Report(i);
                    Thread.Sleep(10);
                    if (i > 950)
                    {
                        end = true;
                    }
                }
                        ((IProgress<int>)progress).Report(1000);

            });
            if (end)
            {
                restaurant.EndDay();
                DayButtonPropertiesChange();
                restaurant.Closed = true;
                DayNumber.Content = "Day: " + restaurant.NumberOfDay;
            }
        }
        private void StartDayButton_Click(object sender, RoutedEventArgs e)
        {
            if (ListOfEmployee.Items.Count > 0)
            {
                restaurant.StartDay((Tips+=tips.Next(0,15))+IncomesToRestaurant.Next(30, 150));
                TipsLabel.Content = "Tips: " + Tips + " $ ";
                DayButtonPropertiesChange();
                InfoLabel.Content = "Dinner is served! Enjoy your meal!";
                if (restaurant.Closed )
                {
                    restaurant.Closed =  false;
                    DayProgress();
                }
            }
            else
                InfoLabel.Content = "Add an employee";
            }
        private void EndDayButton_Click(object sender, RoutedEventArgs e)
        {
            restaurant.EndDay();
            DayButtonPropertiesChange();
            restaurant.Closed = true;
            DayNumber.Content = "Day: " + restaurant.NumberOfDay;
        }
        private void RemoveButton_Click(object sender, RoutedEventArgs e)
        {
            var looser = ListOfEmployee.SelectedValue as Employee;
            restaurant.FireSomeone(looser);
            ICollectionView view = CollectionViewSource.GetDefaultView(ListOfEmployee.ItemsSource);
            view.Refresh();
        }
        private void HireButton_Click(object sender, RoutedEventArgs e)
        {
            if (EmployeesBox.SelectedValue != null)
            {
                Workers.Worker winner = (Workers.Worker)EmployeesBox.SelectedValue;
                restaurant.Rectuit(winner);
            }
            else
                InfoLabel.Content = "Choose an employee from the list";

            ICollectionView view = CollectionViewSource.GetDefaultView(ListOfEmployee.ItemsSource);
            view.Refresh();
        }
        private void DayButtonPropertiesChange()
        {
            StartDayButton.IsEnabled = !StartDayButton.IsEnabled;
            EndDayButton.IsEnabled = !EndDayButton.IsEnabled;
            HireButton.IsEnabled = !HireButton.IsEnabled;
            RemoveButton.IsEnabled = !RemoveButton.IsEnabled;
            if (!restaurant.Closed)
            {
                StatusLabel.Content = "Closed";
                InfoLabel.Content = " ";
                RestaurantIncomes.Content = "Restaurant incomes: " + restaurant.Incomes + " $ ";
                Tips = 0;
                TipsLabel.Content = "Tips: " + Tips + " $ ";
            }
            else
            {
                StatusLabel.Content = "Opened";
            }
            ICollectionView view = CollectionViewSource.GetDefaultView(ListOfEmployee.ItemsSource);
            view.Refresh();
        }
        private void ResetButton_Click(object sender, RoutedEventArgs e)
        {
           
            restaurant = null;
            restaurant = new RestaurantPlace
            {
                Closed = true
            };
            Default();
        }
    }
}
